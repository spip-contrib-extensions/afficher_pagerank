<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cjconciliateurs_description' => 'Ce plugin affiche le PageRank Google de votre site sur la page &#171;A suivre&#187;.',
	'cjconciliateurs_nom' => 'PageRank',
	'cjconciliateurs_slogan' => 'Afficher le PageRank Google',
);

?>
